//
//  plistReader.swift
//  peopleList
//
//  Created by Mounika Nerella on 8/15/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation
enum plistErrors: Error{
    case readError
}
struct Person{
    let firstname: String
    let lastname: String
    let age: String
    let imageName: String?
}
struct  plistReader {
    static func readplist() throws -> [Person]?{
        var people = [Person]()
        guard let plistPath = Bundle.main.path(forResource: "PList", ofType: "plist")else {
           
        throw plistErrors.readError
            
        }
        print(plistPath)
        guard let plistArray = NSArray(contentsOfFile: plistPath)else{ throw plistErrors.readError}
        
             print (plistArray)
        
    
    for item in plistArray {
    if let person = item as? [String: String]{
        people.append(Person(firstname: person["firstname"]!,lastname: person["lastname"]!, age: person["age"]!,imageName: person["image"]))
       
    }
    }
    return people
    }
}
